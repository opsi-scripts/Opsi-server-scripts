#This scripts lists the ram-configuration of everyclient that run hw-scan before

clients=$(opsi-admin -dS method getClients_listOfHashes | grep hostId | cut -b 8-)
for name in $clients
do
   echo $name >> /home/opsiproducts/server-scripts/ram.txt
   opsi-admin -dS method getHardwareInformation_hash $name | grep "System memory\|System Memory" | cut -d, -f3,5 >> /home/opsiproducts/server-scripts/ram.txt
   opsi-admin -dS method getHardwareInformation_hash $name | grep "Channel[ABCD]-DIMM0\|DIMM 1\|DIMM 2\|DIMM 3\|DIMM 4" | cut -d, -f2,9,11,15,22,24,28,35,37,41,48,50 >>  /home/opsiproducts/server-scripts/ram.txt
done