#! /bin/bash

#gets parameter parsed when script is started
scanswname=$1
depotswname=$2

#get Software Version of Depot Package
depotversion=$(opsi-admin -dS method getProduct_hash $depotswname | grep productVersion | cut -b 16-)

#writes Output to txt Files
echo "$depotswname is on the Depot with Version $depotversion" > "/home/opsiproducts/server-scripts/$scanswname-update.txt"
echo "Following Clients need to update $scanswname" >> "/home/opsiproducts/server-scripts/$scanswname-update.txt"
echo "$scanswname isnt installed on following Clients" > "/home/opsiproducts/server-scripts/$scanswname-notinstalled.txt"

#get list of clients and does the tasks for every client listed
clients=$(opsi-admin -dS method getClients_listOfHashes | grep hostId | cut -b 8-)
for name in $clients
do
  #get installedversion of software (if found in sfaudit)
  installedversion=$(opsi-admin -dS method getSoftwareInformation_hash $name | grep $scanswname | cut -d, -f8 | cut -b 22-)
  if [[ $installedversion = *[!\ ]* ]]; then
    installedversion=${installedversion:0:-1}

    #checks if the installed version is equal to the Version on Depot
    if [ "$installedversion" != "$depotversion" ]; then
      echo "$name curenntly got Version: $installedversion installed" >> "/home/opsiproducts/server-scripts/$scanswname-update.txt"
      #first we set the Product Informations on the Server for the client correctly
      #to directly set the Product to "Setup", just change "0" to "Setup"
      opsi-admin -dS method setProductState $depotswname $name installed "0" $installedversion
      
      #to push it "on-demand" just add:
      #opsi-admin -dS method hostControlSafe_fireEvent on_demand $name
    else
      echo "$name needs no update!"
      #writes Installationstatus and version number to the clients productlist
      opsi-admin -dS method setProductState $depotswname $name installed "0" "$installedversion"
    fi
  else
    echo "$scanswname is not installed at $name" >> "/home/opsiproducts/server-scripts/$scanswname-notinstalled.txt"
  fi

done