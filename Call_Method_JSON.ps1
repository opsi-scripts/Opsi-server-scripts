﻿#This script is used to set opsi-products per json-call from another computer
$urlJSON = 'https://opsi:4447/rpc'

$authUser = ""
$authPass = ""
 
$method = "hostControl_uptime"
#$params = @("<localboot-product>","<client>","setup")
$params =@("")
 
Function CallJSON($url,$object,$authUser,$authPass,$method,$params) {
    $cred = New-Object System.Net.NetworkCredential -ArgumentList $authUser,$authPass
 
    $bytes = [System.Text.Encoding]::ascii.GetBytes($object)
    $web = [System.Net.WebRequest]::Create($url)
 
    [System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}
 
    $web.Method = "POST"
    $web.ContentLength = $bytes.Length
    $web.ContentType = "application/json"
    $web.Credentials = $cred
    $stream = $web.GetRequestStream()
    $stream.Write($bytes,0,$bytes.Length)
    $stream.close()
    $reader = New-Object System.IO.Streamreader -ArgumentList $web.GetResponse().GetResponseStream()
    return $reader.ReadToEnd()| ConvertFrom-Json
    $reader.Close()
}
 
$data = (New-Object PSObject |
    Add-Member -PassThru NoteProperty method $method |
    Add-Member -PassThru NoteProperty params $params |
    Add-Member -PassThru NoteProperty id '1') | ConvertTo-Json -depth 3
 
$request = CallJSON $urlJSON $data $authUser $authPass $method $params